%include "lib.inc"
%define ADDRESS_SIZE 8
%define VALUE_ADDRESS_SIZE 8
global find_word
global get_word
section .text
	find_word:
	;rdi - указатель на строку
	;rsi - указатель на начало списка
	push rbp
	xor rbp, rbp
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11, rsi
		mov ebp, dword[rsi]
		lea rsi, [rsi+ADDRESS_SIZE]
		call string_equals
		test rax, rax
		jne .find
		test rbp, rbp
		je .not_find
		mov esi, ebp
		jmp .loop
	.not_find:
		xor rax, rax
		jmp .r
	.find:
		mov rax, r11
	.r:
		pop r10
	ret

	get_word:
   		mov r10, rdi
    		lea rdi, [rdi + VALUE_ADDRESS_SIZE]
    		call string_length
    		lea rdi, [rdi + rax]
		call skip_cringe
		mov rax, rdi
   	ret
		
		
